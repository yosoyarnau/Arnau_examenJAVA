/**
 * 
 */
package Main;
import javax.swing.JOptionPane;
import Proyecto.Empleado;
/**
 * @author Usuario
 *
 */
public class MainApp {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//Por defecto
		Empleado Empleado= new Empleado();	
		JOptionPane.showMessageDialog(null,"Nombre "+Empleado.getNombre()+"\nCedula "+Empleado.getCedula()+"\nEdad " +Empleado.getEdad()+"\nCasado "+Empleado.isCasado() +"\nSueldo " +Empleado.getSalario()+"\nNivel "+Empleado.getNivel());
		
		
		//Con las opciones especificadas	
		Empleado Empleado2= new Empleado("Carlos","algo",7,true,65);
		JOptionPane.showMessageDialog(null,"Nombre "+Empleado2.getNombre()+"\nCedula "+Empleado2.getCedula()+"\nEdad " +Empleado2.getEdad()+"\nCasado "+Empleado2.isCasado() +"\nSueldo " +Empleado2.getSalario()+"\nNivel "+Empleado2.getNivel());
	
		
		
		//sublase por defecto 
		Empleado Empleado3= new Empleado("Carlos","algo",25,true,65);
		JOptionPane.showMessageDialog(null,"Nombre "+Empleado3.getNombre()+"\nCedula "+Empleado3.getCedula()+"\nEdad " +Empleado3.getEdad()+"\nCasado "+Empleado3.isCasado() +"\nSueldo " +Empleado3.getSalario()+"\nNivel "+Empleado3.getNivel()+"\nCodigo por hora "+);
	
	
		/* FALTA:
		 * 
		 * relacionar la sublase con el main
		 * metodo para el sueldo
		 * 
		 * 
		 */
	
	
	}

}
